### 起動例

ローカルのlocalstackとやり取りする場合。  
AWSと通信する場合は`APP_ENDPOINT_URL`の設定は不要。

```bash
export APP_ENDPOINT_URL=http://localhost:4566
export COPILOT_SNS_TOPIC_ARNS={"sample-sns":"arn:aws:sns:ap-northeast-1:000000000000:sample-sns"}
./gradlew bootRun
```

### API経由でメッセージをSNSに発行

```sh
curl -X POST -H "Content-Type: application/json" -d '{"message":"java"}'  http://localhost:8080/send-message-api/send
```

[トピックへのメッセージの発行](https://docs.aws.amazon.com/ja_jp/sdk-for-java/latest/developer-guide/examples-simple-notification-service.html#sns-publish-message-topic)

### localstackで構築及び動作確認

#### Dockerでlocalstackを起動

```sh
docker run --rm --name docker-localstack -it -p 4566:4566 -p 4571:4571 -d localstack/localstack:0.13.3
```

#### キューを作成

```sh
aws --endpoint-url=http://localhost:4566 sqs create-queue --queue-name sample-que
```

次のような出力になる。

```json
{
  "QueueUrl": "http://localhost:4566/000000000000/sample-que"
}
```

#### キューの一覧を確認

```sh
aws --endpoint-url=http://localhost:4566 sqs list-queues
```

次のような出力になる。

```json
{
  "QueueUrls": [
    "http://localhost:4566/000000000000/sample-que"
  ]
}
```

作成したキューからメッセージを受け取ってみる。

```sh
aws --endpoint-url=http://localhost:4566 sqs receive-message --queue-url http://localhost:4566/000000000000/sample-que
```

当然現時点では何も取得できない。

#### SNSのトピックを作成する

```sh
aws --endpoint-url=http://localhost:4566 sns create-topic --name sample-sns
```

次のような出力になる。

```json
{
  "TopicArn": "arn:aws:sns:ap-northeast-1:000000000000:sample-sns"
}
```

#### SNS サブスクリプションにSQSを登録する

`topic-arn`にはSNSのトピックを作成したときに出力されたarnを指定する。  
`notification-endpoint`にはSQSのキューを作成したときに出力されたURLを指定する。

```sh
aws --endpoint-url=http://localhost:4566 sns subscribe --topic-arn arn:aws:sns:ap-northeast-1:000000000000:sample-sns --protocol sqs --notification-endpoint http://localhost:4566/000000000000/sample-que
```

次のコマンドで登録されたことを確認できる。

```sh
aws --endpoint-url=http://localhost:4566 sns list-subscriptions
```

#### SNSにメッセージを送り動作を確認する

```sh
aws --endpoint-url=http://localhost:4566 sns publish  --topic-arn arn:aws:sns:ap-northeast-1:000000000000:sample-sns --message '{"message":"java"}'
```

```sh
aws --endpoint-url=http://localhost:4566 sqs receive-message --queue-url http://localhost:4566/000000000000/sample-que
```

これで次のようにメッセージを取得できる。

```json
{
  "Messages": [
    {
      "MessageId": "0093d452-26b0-9af3-bac7-9836595399a4",
      "ReceiptHandle": "ongiwiaapmyobxfatuzbutzvnalecxdlsywgkrwcstjmtknkksszqtumlsgmnlocbvjzwjacstcrssrsanohvwgjjvllfogdayqtyxggkfupousbjyjoqhkicumjwxkncnwqozawwqelyeadkjuoezrrehfntuyewluemwenuisqvwjzicukkkeeo",
      "MD5OfBody": "74fa32171e516ebbc1663febd6764552",
      "Body": "{\"Type\": \"Notification\", \"MessageId\": \"9dbd9bbe-aaf5-4b61-b53a-bee1ceedfa0b\", \"TopicArn\": \"arn:aws:sns:ap-northeast-1:000000000000:sample-sns\", \"Message\": \"{\\\"message\\\":\\\"java\\\"}\", \"Timestamp\": \"2022-02-01T17:51:25.374Z\", \"SignatureVersion\": \"1\", \"Signature\": \"EXAMPLEpH+..\", \"SigningCertURL\": \"https://sns.us-east-1.amazonaws.com/SimpleNotificationService-0000000000000000000000.pem\"}"
    }
  ]
}
```

#### SQSコマンドメモ

```bash
aws --endpoint-url=http://localhost:4566 sqs get-queue-attributes --queue-url http://localhost:4566/000000000000/sample-que --attribute-names All
```
