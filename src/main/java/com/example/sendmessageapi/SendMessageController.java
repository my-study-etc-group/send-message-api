package com.example.sendmessageapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;

@Slf4j
@RestController
public class SendMessageController {
    @Value("${copilot.sns.topic.arns}")
    private String topicArn;

    private final SnsClient snsClient;

    public SendMessageController(SnsClient snsClient) {
        this.snsClient = snsClient;
    }

    @PostMapping("/send")
    public String send(@RequestBody Message body) throws JsonProcessingException {
        log.info("RequestBody is " + body.toString());

        ObjectMapper mapper = new ObjectMapper();
        String message = mapper.writeValueAsString(body);

        CopilotSnsTopicArns arn = mapper.readValue(topicArn, CopilotSnsTopicArns.class);

        PublishRequest request = PublishRequest.builder()
            .message(message)
            .topicArn(arn.sampleSns())
            .build();

        PublishResponse result = snsClient.publish(request);
        log.info(result.messageId() + " Message sent. Status is " +
            result.sdkHttpResponse().statusCode());

        return "success!";
    }
}
