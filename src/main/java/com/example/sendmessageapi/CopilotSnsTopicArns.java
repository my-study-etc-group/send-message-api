package com.example.sendmessageapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public record CopilotSnsTopicArns(
    @JsonProperty("sample-sns")
    String sampleSns
) {
}
