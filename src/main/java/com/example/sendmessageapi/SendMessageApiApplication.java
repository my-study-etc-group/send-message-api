package com.example.sendmessageapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SendMessageApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SendMessageApiApplication.class, args);
    }

}
