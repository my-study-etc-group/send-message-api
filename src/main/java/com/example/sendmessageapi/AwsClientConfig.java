package com.example.sendmessageapi;

import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;

@Component
public class AwsClientConfig {
    @Value("${app.endpoint.url:#{null}}")
    private String endpointUrl;

    @Bean
    public SnsClient snsClient() throws URISyntaxException {
        if (endpointUrl == null) {
            return SnsClient.builder()
                .region(Region.AP_NORTHEAST_1)
                .build();
        } else {
            return SnsClient.builder()
                .region(Region.AP_NORTHEAST_1)
                .endpointOverride(new URI(endpointUrl))
                .build();
        }


    }
}
